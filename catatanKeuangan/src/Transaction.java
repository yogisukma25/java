// Kelas untuk merepresentasikan transaksi keuangan
class Transaction {
    private double amount;
    private String date;
    private String category;
    private String description;

    // Konstruktor
    public Transaction(double amount, String date, String category, String description) {
        this.amount = amount;
        this.date = date;
        this.category = category;
        this.description = description;
    }

    // Getter untuk amount
    public double getAmount() {
        return amount;
    }

    // Getter untuk date
    public String getDate() {
        return date;
    }

    // Getter untuk category
    public String getCategory() {
        return category;
    }

    // Getter untuk description
    public String getDescription() {
        return description;
    }
}