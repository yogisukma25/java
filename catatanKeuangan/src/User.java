import java.util.Random;

// Kelas untuk merepresentasikan pengguna
class User {
    private String email;
    private String password;
    private String otp;

    // Konstruktor
    public User(String email, String password) {
        this.email = email;
        this.password = password;
        // Generate OTP
        generateOTP();
        System.out.println(this.otp);
    }

    private void generateOTP() {
        // Panjang OTP yang diinginkan
        int otpLength = 6;
        // Karakter yang diizinkan dalam OTP
        String allowedChars = "0123456789";
        StringBuilder otpBuilder = new StringBuilder();

        Random random = new Random();
        for (int i = 0; i < otpLength; i++) {
            // Pilih karakter acak dari daftar karakter yang diizinkan
            int index = random.nextInt(allowedChars.length());
            otpBuilder.append(allowedChars.charAt(index));
        }

        // Set OTP yang dihasilkan
        this.otp = otpBuilder.toString();
    }

    // Metode untuk memeriksa OTP
    public boolean checkOTP(String otp) {
        return this.otp.equals(otp);
    }
}

