import java.util.Scanner;

// Kelas utama untuk mengatur alur program
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Registrasi pengguna
        System.out.println("Registrasi:");
        System.out.print("Email: ");
        String email = scanner.nextLine();
        System.out.print("Password: ");
        String password = scanner.nextLine();

        User user = new User(email, password);

        // Verifikasi OTP
        System.out.println("Masukkan kode OTP yang telah dikirim ke email Anda:");
        String otp = scanner.nextLine();
        if (!user.checkOTP(otp)) {
            System.out.println("Kode OTP salah. Silakan coba lagi.");
            return;
        }

        // Tampilkan menu tunai dan non tunai
        System.out.println("Pilih jenis transaksi:");
        System.out.println("1. Tunai");
        System.out.println("2. Non Tunai");
        int choice = scanner.nextInt();
        scanner.nextLine(); // Membuang karakter newline

        switch (choice) {
            case 1:
                handleCashTransaction(scanner);
                break;
            case 2:
                handleNonCashTransaction(scanner);
                break;
            default:
                System.out.println("Pilihan tidak valid");
        }
    }

    // Metode untuk menangani transaksi tunai
    private static void handleCashTransaction(Scanner scanner) {
        // Implementasi transaksi tunai
        System.out.println("Masukkan jumlah uang tunai:");
        double cashAmount = scanner.nextDouble();
        scanner.nextLine(); // Membuang karakter newline

        System.out.println("Masukkan tanggal transaksi (format: yyyy-mm-dd):");
        String date = scanner.nextLine();

        System.out.println("Masukkan kategori transaksi:");
        String category = scanner.nextLine();

        System.out.println("Masukkan keterangan transaksi:");
        String description = scanner.nextLine();

        // Buat objek CashTransaction berdasarkan input pengguna
        CashTransaction cashTransaction = new CashTransaction(cashAmount, date, category, description, cashAmount);

        // Outputkan detail transaksi tunai yang baru dibuat
        System.out.println("Transaksi tunai berhasil dicatat:");
        System.out.println(cashTransaction);
    }

    // Metode untuk menangani transaksi non tunai
    private static void handleNonCashTransaction(Scanner scanner) {
        // Implementasi transaksi non tunai
        System.out.println("Masukkan jumlah uang non-tunai:");
        double cashAmount = scanner.nextDouble();
        scanner.nextLine(); // Membuang karakter newline
        System.out.println("Masukkan tanggal transaksi (format: yyyy-mm-dd):");
        String date = scanner.nextLine();

        System.out.println("Masukkan kategori transaksi:");
        String category = scanner.nextLine();

        System.out.println("Masukkan keterangan transaksi:");
        String description = scanner.nextLine();

        // Buat objek CashTransaction berdasarkan input pengguna
        CashTransaction cashTransaction = new CashTransaction(cashAmount, date, category, description, cashAmount);

        // Outputkan detail transaksi non-tunai yang baru dibuat
        System.out.println("Transaksi non-tunai berhasil dicatat:");
        System.out.println(cashTransaction);
    }
}
