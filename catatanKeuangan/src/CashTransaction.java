// Kelas untuk merepresentasikan transaksi tunai
class CashTransaction extends Transaction {
    private double cashAmount;

    // Konstruktor
    public CashTransaction(double amount, String date, String category, String description, double cashAmount) {
        super(amount, date, category, description);
        this.cashAmount = cashAmount;
    }

    // Getter untuk jumlah uang tunai
    public double getCashAmount() {
        return cashAmount;
    }

    // Metode untuk menampilkan detail transaksi tunai
    @Override
    public String toString() {
        return
        "Cash Transaction:\n" +
        "Amount: " + getAmount() + "\n" +
        "Date: " + getDate() + "\n" +
        "Category: " + getCategory() + "\n" +
        "Description: " + getDescription() + "\n" +
        "Cash Amount: " + cashAmount;
    }
}

